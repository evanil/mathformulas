package main.java;

import java.io.BufferedReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import static main.java.Descriptions.getDescriptions;
import static main.java.Formula.newFormula;

public class Formulas {


	private  final Map<String, Formula> formulas;
	private final String formulasFiles;
	private final String formulasDescriptions;
	private final Descriptions descriptions ;

	public Formulas(String formulasFiles, String formulasDescriptions) throws IOException {
		 
		this.formulasFiles = formulasFiles;
		this.formulasDescriptions=formulasDescriptions;
		this.descriptions = getDescriptions(formulasDescriptions);
		this.formulas =  fullFormulaDetails(formulasFiles);

	}
	

	private HashMap<String, Formula> AllFormulas(String formulasFiles) throws IOException{
		
		
		 HashMap<String, Formula> variables =  new HashMap<String, Formula>();

	   	 BufferedReader reader = new BufferedReader(new FileReader(formulasFiles));
	
	   	 String line = null, formula = "";
	   	 
	   	 while ((line = reader.readLine()) != null) {			
       	
	   		 if(line.indexOf("=")!=-1)
	   		 {
   	
	   			 if(formula.length()>0){

	   				 variables.remove(firstVariable(formula).get(0));
	   				 variables.put(firstVariable(formula).get(0), newFormula(firstVariable(formula).get(0), formula, "", null, getVariableNames(formula, false), null, getDepDescription(getVariableNames(formula, false))));

	   			 }


	   			 formula=line;

	   		 }        	 
	   		 else{
	       			 
	   				if(line.indexOf("+")!=-1 || line.indexOf("-")!=-1 || line.indexOf("*")!=-1 || line.indexOf("/")!=-1)
	   			 
	       				 formula=formula+line.replaceAll(" ", "");
	   			
	   		 }
	   	 
	}
	   	 
	       	
	   	if(formula.length()>0){
	   		
				 variables.remove(firstVariable(formula).get(0));
				 variables.put(firstVariable(formula).get(0), newFormula(firstVariable(formula).get(0), formula, "", null, getVariableNames(formula, false), null, getDepDescription(getVariableNames(formula, false))));

			 }
	   	 
		reader.close();
			
		return variables;
	}

	private Map<String, String> getDepDescription(ArrayList<String> variable){

		Map<String, String> desc_dependant =  new HashMap<String, String>();


		for (String temp : variable) {


			if(descriptions.getDescriptions().get(temp)!=null){

					desc_dependant.put(temp,descriptions.getDescriptions().get(temp).getDescription());
			
			}
			else{
			
				desc_dependant.put(temp, " ");
			
			}
		
		}
		return desc_dependant;		
		
	}
	
	private Formula singleComponent(String variable, Map<String, Formula> formulas) throws IOException{
		
		Formula object = formulas.get(variable);
				
		Formula temp;
		
		String description = null;
		
		ArrayList<String> components = new ArrayList<String>();
		
		Map<String, String> desc_components =  new HashMap<String, String>();

		
		for (Map.Entry<String, Formula> entry : formulas.entrySet()) {
						
			temp =  (Formula) entry.getValue();
			
			
	        if(temp.getDependant().contains(variable))
	        {
	        		    
	        	
	    		if(descriptions.getDescriptions().get(temp.getVariable_name())!=null){

		        	components.add(temp.getVariable_name());
	    			desc_components.put(temp.getVariable_name(),descriptions.getDescriptions().get(temp.getVariable_name()).getDescription());
	    			
	    		}
	    		else{
	    			
	    			desc_components.put(temp.getVariable_name(), " ");
	    			
	    		}
	        }
	        

		}
		
		if(descriptions.getDescriptions().get(variable)!=null){

				description=descriptions.getDescriptions().get(variable).getDescription();
		}
		
		
    	return newFormula(variable, object.getFormula(), description, components, object.getDependant(), desc_components, object.getDepDescriptions());
    	
	}
	
	
	private  Map<String, Formula> fullFormulaDetails(String formulasFiles) throws IOException{
			 
	Map<String, Formula> formulas =AllFormulas(formulasFiles);
	
	Map<String, Formula> newFormulas =  new HashMap<String, Formula>();
	Formula temp;

	for (Map.Entry<String, Formula> entry : formulas.entrySet()) {

		temp =  (Formula) entry.getValue();

         newFormulas.put(temp.getVariable_name(), singleComponent(temp.getVariable_name(), formulas));
         
	}

    return newFormulas;

}

	  private static ArrayList<String> firstVariable(String input) {
	        
	    	ArrayList<String> variables = new ArrayList<String>();

	    	Pattern p = Pattern.compile("([a-zA-Z]+(\\d)+)+|[a-zA-Z]+");
   	
	    	if(!input.startsWith("log")){
	    			    		
	    		input=input.substring(0, input.indexOf("="));

	    	}
	    	else{
	    	
	    		input=input.replace("log(", "");
	    		input=input.replace(")", "");
	    		input=input.substring(2, input.indexOf("="));
	    	
	    	}

	    	
	    	Matcher m = p.matcher(input);

	    	String temporal; 
	    	while ( m.find() ) {
	    		temporal=input.substring(m.start(), m.end());
	    		variables.add(temporal);
	    	}
	    	
	    	
	    	return variables;
	    	
	    }
	  
	  private static ArrayList<String> getVariableNames(String input, boolean full) {
	    		        
	    	ArrayList<String> NiGEM_Variables = new ArrayList<String>();

	    	Pattern p = Pattern.compile("([a-zA-Z]+(\\d)*)+");
	    	input=input.substring(input.indexOf("=")+1);

	    	Matcher m = p.matcher(input);

	    	String temporal; 
	    	while ( m.find() ) {
	    		
	    		temporal=input.substring(m.start(), m.end());
	    			    		
	    		if(temporal.indexOf("(")!=-1 && !full)
	    		temporal=temporal.substring(0,temporal.indexOf("("));
	    			    
	    		
	    		if(!NiGEM_Variables.contains(temporal) && !temporal.equals("log"))
	    		NiGEM_Variables.add(temporal);
	    	}
	    	
	    	
	    	return NiGEM_Variables;
	    	
	    }
	  
		public String getFormulasFiles() {
			return formulasFiles;
		}


		public String getFormulasDescriptions() {
			return formulasDescriptions;
		}


		public Descriptions getNigem() {
			return descriptions;
		}

		
			
	public Map<String, Formula> getFormulas() {
		return formulas;
	}




	
	   
}

package main.java;

public class Description {

    private final int number;
    private final String code; 
    private final String value;
    private final String type;
    private final String category;
    private final String description;
    private final String name;

    public static Description newDescription(String name, int number, String code, String value,
			String type, String category, String description) {
    	
    	return new Description( name, number,  code,  value, type,  category,  description);
    	
    }
    
	private Description(String name, int number, String code, String value,
			String type, String category, String description) {

		this.number = number;
		this.code = code;
		this.value = value;
		this.type = type;
		this.category = category;
		this.description = description;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public int getNumber() {
		return number;
	}


	public String getValue() {
		return value;
	}


	public String getType() {
		return type;
	}



	public String getCategory() {
		return category;
	}


	public String getDescription() {
		return description;
	}
    
	
	public String getName() {
		return name;
	}


    
	 
}

package main.java;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.StringTokenizer;

import static main.java.Description.newDescription;

public class Descriptions {

   private final HashMap <String, Description> descriptions;
   private final String path;
   
   public String getVSGEM_path() {
	   return path;
   }

   public static Descriptions getDescriptions(String vSGEM_path) throws IOException {
   	
   	return new Descriptions(vSGEM_path);
   	
   }

   private Descriptions(String vSGEM_path) throws IOException {
	    
	   	 this.path = vSGEM_path;
	   	 this.descriptions = mapOfDescriptions(vSGEM_path);
	   	 
   } 
   
   private HashMap <String, Description> mapOfDescriptions(String VSGEM_path) throws IOException{
	   
	   String code=null, name = null, description=null;
	   	 
	   	 HashMap <String, Description> variables = new HashMap<String, Description>();
	   	
	   	 BufferedReader reader = new BufferedReader(new FileReader(VSGEM_path));
	
	   	 String line = null;

	   	 while ((line = reader.readLine()) != null) {
			
			StringTokenizer st = new StringTokenizer(line);
			 
			int i=0;
			
			while (st.hasMoreElements() && i<=4) {
				

				switch(i){
				
					case 0: 	{
						String temp= st.nextToken();
										
						code = code(temp);
						name = temp.substring(code(temp).length());
						
						break;
					}
									
					case 1: 	{
						String temp= st.nextToken();

						description=line.substring(line.indexOf(temp));
						
						break;
					}

				
					default:
						st.nextToken();
						break;
					}
				
					i++;
				}

			variables.put(name, newDescription(name, 0, code, "", "", "", description));

		}
	   	 
		reader.close();
		
		return variables;
   }
   
   private static String code(String str){

		String result ="";  
	            
	    for (int i=0; i<str.length(); i++){  
	                          
	    	Character chars = str.charAt(i);  
	    	if(Character.isDigit(chars))  
	    		{  
	    			result += chars;  
	    		}  
	    }  

	  	return result;
	}

   

   public HashMap<String, Description> getDescriptions() {
	   return descriptions;
   }


   
}

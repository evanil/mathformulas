package main.java;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class Formula {
	
	 private final String variable_name;
	 private final String formula;
	 private final String description;
	 private final ArrayList<String> components;
	 private final ArrayList<String> dependant;
	 private final Map<String, String> compDescriptions;
	 private final Map<String, String> depDescriptions;
	 
	 public static Formula newFormula(String variable_name, String formula, String description, ArrayList<String> components,
			 ArrayList<String> dependant, Map<String, String> compDescriptions, Map<String, String> depDescriptions) throws IOException {
		   	
		   	return new Formula( variable_name,  formula,  description,  components, dependant,  compDescriptions, depDescriptions);
		   	
	 }

	 
	 private Formula(String variable_name, String formula, String description, ArrayList<String> components,
			 ArrayList<String> dependant, Map<String, String> compDescriptions, Map<String, String> depDescriptions){
		 
		 this.variable_name=variable_name;
		 this.formula=formula;
		 this.description=description;
		 this.components=components;
		 this.dependant  = dependant;
		 this.compDescriptions  = compDescriptions;
		 this.depDescriptions = depDescriptions;
		 
	 }
	 public String getVariable_name() {
	
		 return variable_name;

	 }
	public String getFormula() {
	
		return formula;
	}

	public ArrayList<String> getComponents() {

		return components;

	}
	public ArrayList<String> getDependant() {
		return dependant;
	}
	
	 public String getDescription() {
		return description;
	}
	public Map<String, String> getCompDescriptions() {
		return compDescriptions;
	}
	public Map<String, String> getDepDescriptions() {
		return depDescriptions;
	}


}

package json;

import java.io.IOException;
import java.util.Map;

import main.java.Formula;




public interface MathJson {
	
		Formula getFormula(String variable) throws IOException;
		void generateJson(Map<String, Formula> map ) throws IOException;
}

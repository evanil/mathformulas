package json;

import static main.java.Formula.newFormula;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import main.java.Formula;

public class MathCollection implements MathJson {


	@Override
	public Formula getFormula(String variable) throws IOException {
		ArrayList<String> test = new ArrayList<String>();
		ArrayList<String> test1 = new ArrayList<String>();
		Map<String, String> descr = new HashMap<String, String>();
		Map<String, String> descr1 = new HashMap<String, String>();

		String name, dependant_name, component_name;
		
		String mathName = null, formula= null, description= null;
		
		try {
	    		JsonReader reader = new JsonReader(new FileReader("test.txt"));
	    	 
	    		reader.beginObject();
	    	 
	    		while (reader.hasNext()) {
	    	 
	    			   name = reader.nextName();
                  
	    			  if(name.equals(variable)){
	    				 
	    				  reader.beginObject();

	    			while (reader.hasNext()) {
	    				reader.nextName();
	    				mathName=reader.nextString();
	    				reader.nextName();
	    				formula=reader.nextString();
	    				reader.nextName();
	    				description=reader.nextString();

			    		// read components
			    		
	    				reader.nextName();

			    		reader.beginArray();
	    				test1.clear();

			    		while (reader.hasNext()) {
			    			
			    			component_name = reader.nextString();

			    			if(!component_name.equals(variable))

			    			test1.add(component_name);
			    		}
			    		

			    		reader.endArray();
			    		
			    		// read dependants

			    		reader.nextName();

			    		reader.beginArray();

			    		while (reader.hasNext()) {
			    			dependant_name = reader.nextString();

			    			if(!dependant_name.equals(variable))
			    			test.add(dependant_name);
			    			
			    		}
			     


			    		reader.endArray();				    						    	
			    		
			    		// setCompDescriptions
			    		
	    				reader.nextName();

	    				 reader.beginObject();
			    		descr1.clear();

			    		while (reader.hasNext()) {
			    	     	descr1.put(reader.nextName(), reader.nextString());

			    		}
			    		

			    		reader.endObject();
			    		
			    		// read compDescriptions
			    		
	    				reader.nextName();

	    				 reader.beginObject();
	    				 descr.clear();

			    		while (reader.hasNext()) {
			    	     	descr.put(reader.nextName(), reader.nextString());
			    		}
			    		

			    		reader.endObject();
			    		

	    			}
		    		reader.endObject();
	    			  }
	    			  else{
	    				  
	    					reader.skipValue(); 

	    			  }

    	        }

	    		reader.endObject();
	    		reader.close();
	    	 
	    	   } catch (FileNotFoundException e) {
	    		e.printStackTrace();
	    	     } catch (IOException e) {
	    		e.printStackTrace();
	    	     }
	    
	     
	     			return newFormula(mathName, formula, description, test1, test, descr1, descr);
	     
	    	   }

	@Override
	public void generateJson(Map<String, Formula> map) throws IOException{
		 
			GsonBuilder gsonBuilder=new GsonBuilder().setPrettyPrinting();
	        
			Gson gson=gsonBuilder.create();
	        	       
			String jsonData=gson.toJson(map);
	        		       
			PrintWriter writer = new PrintWriter("test.txt", "UTF-8");

			writer.println(jsonData);
	      
			writer.close();
		
	}


	

}

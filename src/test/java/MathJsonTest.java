package test.java;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.io.IOException;
import java.util.Arrays;

import json.MathCollection;

import main.java.Formulas;

import org.junit.Before;
import org.junit.Test;

public class MathJsonTest {

		private MathCollection test;
		
		@Before
	    public void init() throws IOException {
			
			test = new MathCollection();
	    	
	    }
		
		   @Test
		   public  void testFormulasReading() throws IOException{			   

			   //generate txt file
			   test.generateJson( new Formulas("refactor.txt", "descriptions.txt").getFormulas());

			   // read values
			   assertThat(test.getFormula("x").getDependant(), is(Arrays.asList("a","b")));

		   }
}

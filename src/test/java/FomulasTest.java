package test.java;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.io.IOException;
import java.util.Arrays;


import main.java.Formulas;

import org.junit.Before;
import org.junit.Test;

public class FomulasTest {
	private   Formulas 	test; 

    @Before
    public void init() throws IOException {
    	
         	test = new Formulas("refactor.txt", "descriptions.txt");
    }

   @Test
   public void testFormulas() throws IOException{
     
	   assertThat(test.getFormulas().get("y").getFormula(), is("y= z+c+d+f"));
	   assertThat(test.getFormulas().get("x").getFormula(), is("x = a+ b"));
	   assertThat(test.getFormulas().get("l").getFormula(), is("l = x(-1)"));

   }
   
   @Test
   public void testDependatants() throws IOException{
     
	   assertThat(test.getFormulas().get("y").getDependant(), is(Arrays.asList("z", "c", "d","f")));
	   assertThat(test.getFormulas().get("x").getDependant(), is(Arrays.asList("a","b")));

   }

   @Test
   public void testComponentsDescriptions() throws IOException{
	   
	   assertThat(test.getFormulas().get("x").getComponents(), is(Arrays.asList("l", "m")));
	   
   }
   
   @Test
   public void testGetDescriptions() throws IOException{
	   
	   assertThat(test.getFormulas().get("x").getDescription(), is("whatever"));
	   
   }
   
}

Two files are given. The first file contains several mathematical formulas (refactor.txt) where as 
the second file, for each formula contains its description (descriptions.txt). 

The library after parsing these 2 files returns a JSON file (test.txt) of this form:

 "x": {
    "variable_name": "x",
    "formula": "x = a+ b",
    "description": "whatever",
    "components": [
      "l",
      "m"
    ],
    "dependant": [
      "a",
      "b"
    ],
    "compDescriptions": {
      "l": "7whatever",
      "m": "8whatever"
    },
    "depDescriptions": {
      "b": "4whatever",
      "a": "3whatever"
    }
  }
